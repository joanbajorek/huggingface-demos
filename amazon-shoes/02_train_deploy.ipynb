{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training and deploying models\n",
    "\n",
    "In this notebook, we're going to train a model on our local machine. \n",
    "\n",
    "Then, we'll predict locally, and we'll also deploy the model to the Hugging Face [Inference API](https://huggingface.co/inference-api).\n",
    "\n",
    "* ```transformers``` documentation: https://huggingface.co/docs/transformers/index\n",
    "\n",
    "* ```datasets```documentation: https://huggingface.co/docs/datasets/index\n",
    "\n",
    "\n",
    "This is just a quick overview. For an extensive course on Transformers, please visit https://huggingface.co/course."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1 - Setup"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*If you've already installed Git LFS in a previous notebook, there's no need to repeat the steps below.*\n",
    "\n",
    "In order to a push a model to the Hugging Face Hub, we need to install Git Large File Support (LFS):\n",
    "\n",
    "1) Git LFS setup:\n",
    "\n",
    "In a terminal:\n",
    "\n",
    "```\n",
    "curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | sudo bash\n",
    "sudo yum install git-lfs -y\n",
    "git lfs install\n",
    "```\n",
    "Then, add ```*.csv filter=lfs diff=lfs merge=lfs -text``` to ```.gitattributes```, so that CSV files will also be managed with Git LFS.\n",
    "\n",
    "2) Install the [Hugging Face CLI](https://github.com/huggingface/huggingface_hub)\n",
    "\n",
    "```pip -q install huggingface_hub```\n",
    "\n",
    "3) Login to the Hub with your hub credentials\n",
    "\n",
    "```huggingface-cli login```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%sh\n",
    "pip -q install torch scipy transformers datasets widgetsnbextension ipywidgets huggingface_hub --upgrade"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import transformers\n",
    "import datasets\n",
    "\n",
    "print(transformers.__version__)\n",
    "print(datasets.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2 - Train locally"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from transformers import AutoConfig, AutoModelForSequenceClassification, AutoTokenizer, Trainer, TrainingArguments\n",
    "from sklearn.metrics import accuracy_score, precision_recall_fscore_support\n",
    "from datasets import load_dataset, load_from_disk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we define the name of base model we're going to start from, as well as some usual hyperparameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "base_model_id = 'distilbert-base-uncased'\n",
    "\n",
    "epochs = 1\n",
    "num_labels = 5         # Dataset has 5 classes\n",
    "learning_rate = 5e-5\n",
    "train_batch_size = 32\n",
    "eval_batch_size = 64\n",
    "save_strategy = 'no'\n",
    "save_steps = 1000\n",
    "\n",
    "output_data_dir = \"./output\"\n",
    "model_dir = \"./model\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we load the dataset we processed in the previous notebook. We can either load it from the Hugging Face Hub or from disk."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Option 1: load dataset from the Hub\n",
    "dataset_name = 'juliensimon/amazon-shoe-reviews'\n",
    "dataset = load_dataset(dataset_name)\n",
    "\n",
    "# Option 2: load dataset from local storage\n",
    "#dataset = load_from_disk(\"./data\")\n",
    "\n",
    "print(dataset)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset['train'][0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_dataset = dataset['train']\n",
    "valid_dataset = dataset['test']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we define a function that computes the different metrics that we'd like to log during training."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_metrics(pred):\n",
    "        labels = pred.label_ids\n",
    "        preds = pred.predictions.argmax(-1)\n",
    "        precision, recall, f1, _ = precision_recall_fscore_support(labels, preds)\n",
    "        acc = accuracy_score(labels, preds)\n",
    "        return {\"accuracy\": acc, \"f1\": f1, \"precision\": precision, \"recall\": recall}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we download the base model and its tokenizer from the Hugging Face Hub."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = AutoModelForSequenceClassification.from_pretrained(base_model_id, num_labels=num_labels)\n",
    "tokenizer = AutoTokenizer.from_pretrained(base_model_id)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we define a function to tokenize the datasets. We'll process them in batches to speed things up. We apply this function to both datasets with the ```map()``` function available in the ```datasets``` library."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def tokenize(batch):\n",
    "        return tokenizer(batch['text'], padding='max_length', truncation=True)\n",
    "\n",
    "train_dataset = train_dataset.map(tokenize, batched=True, batch_size=len(train_dataset))\n",
    "valid_dataset = valid_dataset.map(tokenize, batched=True, batch_size=len(valid_dataset))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We remove the ```text``` column as it's not needed anymore. This is an optional step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_dataset = train_dataset.remove_columns(['text'])\n",
    "valid_dataset = valid_dataset.remove_columns(['text'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define the ```TrainingArguments``` for our training job: hyperparameters, where to save the model, etc. \n",
    "\n",
    "Documentation: https://huggingface.co/docs/transformers/main/en/main_classes/trainer#transformers.TrainingArguments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hub_model_id = 'juliensimon/distilbert-amazon-shoe-reviews'\n",
    "\n",
    "training_args = TrainingArguments(\n",
    "        hub_model_id=hub_model_id,   # This is where we'll push the model after training\n",
    "        output_dir=model_dir,\n",
    "        num_train_epochs=epochs,\n",
    "        per_device_train_batch_size=train_batch_size,\n",
    "        per_device_eval_batch_size=eval_batch_size,\n",
    "        save_strategy=save_strategy,\n",
    "        save_steps=save_steps,\n",
    "        evaluation_strategy=\"epoch\",\n",
    "        learning_rate=learning_rate,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we use the ```Trainer``` object to put all the pieces together.\n",
    "\n",
    "Documentation: https://huggingface.co/docs/transformers/main/en/main_classes/trainer#transformers.Trainer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer = Trainer(\n",
    "        model=model,\n",
    "        args=training_args,\n",
    "        tokenizer=tokenizer,\n",
    "        compute_metrics=compute_metrics,\n",
    "        train_dataset=train_dataset,\n",
    "        eval_dataset=valid_dataset,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We're now ready to train."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer.train()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the model has been trained, we can easily evaluate it with any dataset. Here, we use the validation set again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "trainer.evaluate(eval_dataset=valid_dataset)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3 - Saving the dataset and pushing it to the to Hub"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's save the model locally, and also push it to the Hugging Face Hub."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer.save_model(model_dir+'_local')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer.push_to_hub()\n",
    "\n",
    "# Another way to do this is to set push_to_hub=True in training_args"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model is now visible at https://huggingface.co/juliensimon/distilbert-amazon-shoe-reviews\n",
    "\n",
    "Of course, we could also use a Git workflow to push a model to the hub:\n",
    "\n",
    "```\n",
    "huggingface-cli repo create -y distilbert-amazon-shoe-reviews\n",
    "\n",
    "git clone https://huggingface.co/juliensimon/distilbert-amazon-shoe-reviews\n",
    "\n",
    "cd distilbert-amazon-shoe-reviews\n",
    "\n",
    "cp ../model/* .\n",
    "\n",
    "git add .\n",
    "\n",
    "git commit -m 'Initial version'\n",
    "\n",
    "git push\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's good practice to describe models with a model card: language(s), task types, dataset(s) used for training, etc. \n",
    "\n",
    "If you used ```push_to_hub()```, your model should have a model card already. Feel free to edit it as required.\n",
    "\n",
    "If not, you can easily create a model card by clicking on \"Add a model card\" on the dataset page. Alternatively, you can add a README.md file to the model repository. This file should follow a well-defined format defined at https://huggingface.co/docs/hub/models-cards."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4 - Predict locally with a Transformers pipeline\n",
    "\n",
    "The ```pipeline``` API is the simplest way to predict with a Hugging Face model. We can load our model from the Hugging Face Hub, or from local storage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from transformers import pipeline\n",
    "\n",
    "# Option 1: load model from the Hugging Face Hub\n",
    "classifier = pipeline('text-classification', model=hub_model_id)\n",
    "\n",
    "# Option 2: load model from local storage\n",
    "#classifier = pipeline(\"text-classification\", model=\"./model\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, it's really just a one-liner."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "classifier(\"The shoes fell to pieces after a few days, why did I buy them???\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "classifier(\"This is a very comfortable pair of shoes, I'm super happy with it.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5 - Predict locally with Auto* classes\n",
    "\n",
    "For more control, we can also load and predict with the ```Auto*``` classes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = AutoModelForSequenceClassification.from_pretrained(hub_model_id, num_labels=5)\n",
    "\n",
    "tokenizer = AutoTokenizer.from_pretrained(hub_model_id)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start from a couple of reviews and tokenize them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "raw_inputs = [\n",
    "    \"The shoes fell to pieces after a few days, why did I buy them???\",\n",
    "    \"This is a very comfortable pair of shoes, I'm super happy with it.\"\n",
    "]\n",
    "\n",
    "inputs = tokenizer(raw_inputs, padding=True, truncation=True, return_tensors='pt')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "inputs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We pass the token sequences to the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "outputs = model(inputs['input_ids'])\n",
    "outputs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We apply the softmax function to the output logits, and see that we get the same top probability as with the pipeline above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.special import softmax\n",
    "\n",
    "probs = softmax(outputs.logits.detach().numpy(), axis=1)\n",
    "probs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6 - Deploy and predict with the Inference API"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can easily host the model on infrastructure managed by the [Inference API](https://huggingface.co/inference-api).\n",
    "\n",
    "Don't forget to replace API_TOKEN with your own Hugging Face token. You can find it in your account settings. Remember that this token is stricly personal: do not share it anywhere.\n",
    "\n",
    "![Locating your access token](images/05.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import requests\n",
    "\n",
    "API_URL = \"https://api-inference.huggingface.co/models/{}\".format(hub_model_id)\n",
    "headers = {\"Authorization\": \"Bearer API_TOKEN\"}\n",
    "\n",
    "def query(payload):\n",
    "\tresponse = requests.post(API_URL, headers=headers, json=payload)\n",
    "\treturn response.json()\n",
    "\t\n",
    "query({\n",
    "\t\"inputs\": \"The shoes fell to pieces after a few days, why did I buy them???\",\n",
    "})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "instance_type": "ml.t3.medium",
  "kernelspec": {
   "display_name": "conda_pytorch_p38",
   "language": "python",
   "name": "conda_pytorch_p38"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
